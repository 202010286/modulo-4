using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
5. Un programa que prepare espacio para un máximo de 100 nombres. El usuario deberá ir
introduciendo un nombre cada vez, hasta que se pulse Intro sin teclear nada, momento en
el que dejarán de pedirse más nombres y se mostrará en pantalla la lista de los nombres
que se han introducido.
*/

namespace Ejercicio5
{
    class Program
    {     
        private string[] nombres;
        private int vueltas = 0;
        private int x = 1, y = 1;
        
        
        public void Inicializar()
        {
            
            nombres = new string[100];
            
            
            for (int i = 0; i < 100; i++)
            {
                Console.Write($"Ingresar nombre {x}: ");
                nombres[i] = Console.ReadLine();
                x++;
                
                if (nombres[i] != "")
                {
                    vueltas++;
                }
                
                if (nombres[i] == "")
                {
                    break;
                }
            }
        }
        
        public void Imprimir()
        {
            Console.WriteLine("\nLista de nombres Ingresados: ");
            for (int i = 0; i < vueltas; i++)
            {
                Console.WriteLine($"Nombre {y}: {nombres[i]}");
                y++;
            }
        }
        
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Inicializar();
            p.Imprimir();
        }
    }
}
