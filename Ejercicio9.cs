using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
9. Crear una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. 
Finalizar el programa al ingresar el -1.
*/

namespace Tabla
{
    class TablaMultiplicar
    {
        public void Ingresar_valor()
        {
            int valor;
            do
            {
                Console.Write("Ingrese un valor: ");
                valor = int.Parse(Console.ReadLine());
                if (valor != -1)
                {
                    Calcular(valor);
                }
            } while (valor != -1);
        }

        public void Calcular(int valor_ingresado)
        {
            
            int x = 1;
            Console.Write($"Tabla del {valor_ingresado}");
            for(int i = valor_ingresado; i <= valor_ingresado * 10; i = i + valor_ingresado) 
            {
                Console.Write($"\n{valor_ingresado} x {x} = {i}");
                x++;
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            TablaMultiplicar tm = new TablaMultiplicar();
            tm.Ingresar_valor();
        }
    }
}
