using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
3. Un programa que almacene en un array el número de días que tiene cada mes
(supondremos que es un año no bisiesto), pida al usuario que le indique un mes (1=enero,
12=diciembre) y muestre en pantalla el número de días que tiene ese mes.
*/


namespace Ejercicio3
{
    class Program
    {     
      
        
        static void Main(string[] args)
        {
            int x = 1;
            int entrada;
            string[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" };
            
            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine($"{x} {meses[i]}");
                x++;
            }
            Console.Write("Seleccionar mes: ");
            entrada = int.Parse(Console.ReadLine());
            
            if (entrada == 1 || entrada == 3 || entrada == 5 || entrada == 7 || entrada == 8 || entrada == 9 || entrada == 12)
            {
                Console.Write("El mes selecionado tiene 31 dias.");
            }
            else if (entrada == 2)
            {
                Console.Write("El mes selecionado tiene 28 dias.");
            }
            else
            {
                Console.Write("El mes selecionado tiene 30 dias.");
            }
        }
    }
}
