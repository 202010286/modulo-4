using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
8. Crear un programa que defina un array de 5 elementos de tipo float que representen las alturas de 5 personas.
Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.

*/


namespace Ejercicio8
{
    class PruebaVector
    {
        private float[] alturas;
        private float promedio;
        private int x = 1;

        public void Cargar() 
        {
            alturas = new float[5];
            
            for (int i = 0; i < 5; i++)
            {
                
                Console.Write($"Ingrese la altura de la persona {x}: ");
                alturas[i] = float.Parse(Console.ReadLine());
                x++;
                
            }
        }
    
        public void Promedio() 
        {
            float suma = 0;
            
            for (int i = 0; i < 3; i++) 
            {
                suma = suma + alturas[i];
            }
            promedio = suma / 3;
            Console.WriteLine($"Promedio de alturas: {promedio}");
        }

        public void Control() 
        {
            int mayor = 0, menor = 0;
            
            for(int i = 0; i < 3; i++) 
            {
                if (alturas[i] > promedio) 
                {
	                mayor++;
                }
                else
                {
                    if (alturas[i] < promedio) 
                    {
                        menor++;
                    }
                }
            }
            Console.WriteLine($"Cantidad de personas mayores al promedio: {mayor}");
            Console.WriteLine($"Cantidad de personas menores al promedio: {menor}");
        }

        static void Main(string[] args)
        {
            PruebaVector pv = new PruebaVector();
            pv.Cargar();
            pv.Promedio();
            pv.Control();
        }
    }
}
