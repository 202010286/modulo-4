using System;

namespace Ejercicio2
{
    class Entrada_numeros
    {
        private float[] numeros;

        public void Cargar_numeros()
        {
            numeros = new float[5];
            for (int i = 0; i < 5; i++)
            {
                Console.Write("Ingrese un numero: ");
                numeros[i] = float.Parse(Console.ReadLine());
            }
            int j = 4;
            while (j >= 0)
            {
                Console.WriteLine(numeros[j]);
                j--;
            }

        }


        static void Main(string[] args)
        {
            Entrada_numeros En = new Entrada_numeros();
            En.Cargar_numeros();
        }
    }
}
