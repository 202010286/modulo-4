using System;

namespace Ejercicio4
{
    class Entrada_numeros
    {
        private int[] numeros;

        public void Cargar_numeros()
        {
            numeros = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Console.Write("Ingrese un numero: ");
                numeros[i] = int.Parse(Console.ReadLine());
            }
        }
        public void NumeroMayor()
        {
            float mayor;
            int posicion;
            mayor = numeros[0];
            posicion = 0;
            for (int f = 1; f < numeros.Length; f++)
            {
                if (numeros[f] > mayor)
                {
                    mayor = numeros[f];
                    posicion = f;
                }
            }
            Console.Write($"El numero mas grande es el: {numeros[posicion]}");

        }

        static void Main(string[] args)
        {
            Entrada_numeros En = new Entrada_numeros();
            En.Cargar_numeros();
            En.NumeroMayor();
        }

    }
}
