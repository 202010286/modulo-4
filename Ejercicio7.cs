using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
7. Un programa que te pida tu nombre y lo muestre en pantalla separando cada letra de la
siguiente con un espacio. Por ejemplo, si tu nombre es "Moreta", deberia aparecer en
pantalla "M o r e t a".
*/


namespace Ejercicio7
{
    class Program
    {     
        
        static void Main(string[] args)
        {
            
            string nombre;
            
            Console.Write("Ingresar nombre: ");
            nombre = Console.ReadLine();
            
           foreach (var letras in nombre)
           {
               Console.Write($"{letras} ");
           }
            
        }
    }
}
