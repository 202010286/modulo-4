using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
1. Un programa que pida al usuario 4 números, los memorice (utilizando un array), calcule su
media aritmética y después muestre en pantalla la media y los datos tecleados.
*/

namespace Ejercicio1
{
    class Program
    {
        private int[] numeros;
        private int x = 1, suma = 0, promedio;
        
        public void Carga()
        {
            numeros = new int[4];
            
            for (int i = 0; i < 4; i++)
            {
                Console.Write($"Cargar numero {x}: ");
                numeros[i] = int.Parse(Console.ReadLine());
                x++;
            }
            
            int y = 1;
            Console.Write("\nValores tecleados:");
            Console.WriteLine();
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine($"Valor {y}: {numeros[i]}");
                y++;
            }
        }
        
        
        public void MediaAritmetica()
        {
            for (int i = 0; i < 4; i++)
            {
                suma = suma + numeros[i];
            }
            promedio = suma / 4;
            Console.Write($"\nEl promedio de los valores ingresados es: {promedio}");
        }
        
    
        static void Main(string[] args)
        {
            Program pm = new Program();
            pm.Carga();
            pm.MediaAritmetica();
        }
    }
}
