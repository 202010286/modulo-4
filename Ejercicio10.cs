using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
10. Confeccionar una clase que permita carga el nombre y la edad de una persona. Mostrar
los datos cargados. Imprimir un mensaje si es mayor de edad (edad>=18).
*/


namespace Ejercicio10
{
    class Program
    {     
        
        private string nombre;
        private int edad;
        
        public void Persona()
        {
            
            Console.Write("Ingresar nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su edad: ");
            edad = int.Parse(Console.ReadLine());
        }
        
        public void Imprimir()
        {
            
            Console.WriteLine($"El nombre ingresado es: {nombre}");
            Console.Write($"La edad ingresada es: {edad}");
            
            if (edad >= 18)
            {
                Console.Write($"\nEl usuario {nombre} es mayor de edad con {edad} años.");
            }
            else
            {
                Console.Write($"\nEl usuario {nombre} es menor de edad.");
            }
            
        }
        
        static void Main(string[] args)
        {
            
           Program p = new Program();
           
           p.Persona();
           p.Imprimir();
            
        }
    }
}
